import QueryString from 'querystring'

export const buildQuery = (query, hash) => {
  if (!hash) {
    return query
  }
  const queries = query.split('&').reduce((acc, q) => {
    q = q.split('=')
    if (q.length === 2) {
      acc[q[0]] = q[1]
    }
    return acc
  }, {})
  hash = Object.keys(hash).reduce((acc, key) => {
    acc[key] = encodeURIComponent(hash[key])
    return acc
  }, {})
  const newQueries = Object.assign({}, queries, hash)
  return Object.keys(newQueries).map(key => `${key}=${newQueries[key]}`).join('&')
}

export const buildUrl = (baseUrl, { path, query, hash }) => {
  const queryObj = QueryString.stringify(query || {})
  return `${baseUrl || ''}${path || ''}${queryObj ? '?' + queryObj : ''}${hash ? '#' + hash : ''}`
}

export const idx = (obj, callback = (_obj) => { }) => {
  try {
    return callback(obj)
  } catch (ignore) {
  }
  return undefined
}

export const isArray = (items) => {
  return Array.isArray(items)
}

export const isObject = (item) => {
  return (item && typeof item === 'object' && !isArray(item))
}

export const merge = (...args) => {
  const target = args[0]
  args.filter((value, key) => key > 0).forEach(value => _mergeAPair(target, value))
  return target
}

export const reload = () => {
  window.location.reload()
}

const _mergeAPair = (target, source) => {
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(key => {
      if (isObject(source[key])) {
        if (!target[key]) {
          Object.assign(target, { [key]: {} })
        }
        _mergeAPair(target[key], source[key])
      } else {
        Object.assign(target, { [key]: source[key] })
      }
    })
  }
  return target
}
