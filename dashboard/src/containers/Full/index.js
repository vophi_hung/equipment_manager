import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

import { Header, Sidebar, Breadcrumb, Footer } from '../../components'
import { localStorageModule } from '../../modules'
import { Company, Dashboard, Review, User } from '../../views/'

export default class Full extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  componentDidMount() {
    if (!localStorageModule.isLogin()) {
      this.context.router.history.replace('/login')
    }
  }

  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props} />
          <main className="main">
            <Breadcrumb />
            <div className="container-fluid">
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={Dashboard} />
                <Route path="/tables/company" name="Company" component={Company} />
                <Route path="/tables/review" name="Review" component={Review} />
                <Route path="/tables/user" name="User" component={User} />
                <Redirect from="/" to="/dashboard" />
              </Switch>
            </div>
          </main>
        </div>
        <Footer />
      </div>
    )
  }
}
