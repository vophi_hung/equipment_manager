import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import { adminService } from '../../modules/ApiModule'
import {  localStorageModule } from '../../modules'
import { adminService } from '../../modules/ApiModule'
import FacebookLogin from 'react-facebook-login';

const BG_COLOR = 'rgb(242, 111, 16)'

export default class Login extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  async processLogin(accessToken) {
    const data = await adminService.login(accessToken)
    if(data.type != 'admin')  {
      alert('please login with an admin role')
    } else {
      localStorageModule.setToken(data.token)
      window.location.reload()
    }
    
  }

  componentDidMount() {
    if (localStorageModule.isLogin()) {
      this.context.router.history.replace('/')
    }

    const FB = window.FB
    FB.init({
      appId: '170695930140490',
      cookie: true,  // enable cookies to allow the server to access
                        // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.3' // use version 2.1
    })

    FB.getLoginStatus(response => this.statusChangeCallback(response))
  }
  
  statusChangeCallback(response) {
    if (response.status === 'connected') {
      if (!localStorageModule.isLogin()) {
        this.processLogin(response.authResponse.accessToken)
      }
    } 
  }

  checkLoginState() {
    window.FB.getLoginStatus(function(response) {
      this.statusChangeCallback(response)
    }.bind(this))
  }

  async handleResponse(response) {
    const { accessToken } = response
    if (accessToken) {
      try {
        console.log('process login')
        await this.processLogin(accessToken)
      } catch (error) {
        
      }
    }
    
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8">
              <div className="card-group mb-0">
                <div style={{ borderWidth: 0 }} className="card p-4">
                  <div className="card-block">
                    <h1>Login</h1>
                    <p>Login by your Facebook account</p>
                    <div className="input-group mb-3">
                      <FacebookLogin
                        autoLoad={true}
                        appId="170695930140490"
                        fields="name,email,picture"
                        onClick={this.componentClicked}
                        callback={this.handleResponse} />
                    </div>
                  </div>
                </div>
                <div className="card card-inverse card-primary py-5 d-md-down-none" style={{ width: 44 + '%', backgroundColor: BG_COLOR, border: 'none' }}>
                  <div className="card-block text-center">
                    <div>
                      <img className="img-responsive" alt="" src="./logo_symbol.png" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  _responseGoogle = async (data) => {
    const { id_token: accessToken } = data.getAuthResponse()
    // let { admin, token } = await adminService.login(accessToken)
    // localStorageModule.setAdmin(admin)
    // localStorageModule.setToken(token)
    this.context.router.history.replace('/')
  }
}
