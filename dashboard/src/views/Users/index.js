import React, { Component } from 'react'
import { userService } from '../../modules/ApiModule'
import BaseDataTable from '../BaseDataTable'

const DETAIL_COLUMNS = [{
  name: 'avatar',
  title: 'Avatar'
}, {
  name: 'name',
  title: 'Name'
}, {
  name: 'address',
  title: 'Address'
}]

const LIST_COLUMNS = [{
  name: 'id',
  title: 'ID',
  type: 'text'
}, {
  name: 'social_id',
  title: 'Avatar',
  type: 'custom',
  custom: data => {
    return (
      <a href={`https://facebook.com/${data}`} target={'blank'} >
        <img src={`http://graph.facebook.com/${data}/picture?type=square`} style={{
          maxHeight: 30,
          width: 'auto'
        }} />
      </a>)
  }
}, {
  name: 'name',
  title: 'Name'
}]

export default class User extends BaseDataTable {

  constructor(props) {
    super({
      props,
      Service: userService,
      LIST_COLUMNS,
      DETAIL_COLUMNS
    })
  }
}
