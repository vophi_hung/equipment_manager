import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Form, PaginatedTable } from '../../components'
import { reviewService } from '../../modules/ApiModule'
import * as utils from '../../utils'
import BaseDataTable from '../BaseDataTable'

const DETAIL_COLUMNS = [{
  name: 'user_name',
  title: 'User',
  type: 'text'
}, {
  name: 'company',
  title: 'Company',
  type: 'text'
}, {
  name: 'general',
  title: 'General'
}, {
  name: 'cons',
  title: 'Cons'
}, {
  name: 'pros',
  title: 'Pros'
}, {
  name: 'mess_to_manager',
  title: 'Mess to manager'
}, {
  name: 'status',
  title: 'Status',
  type: 'boolean'
}]

const LIST_COLUMNS = [{
  name: 'user_name',
  title: 'User',
  type: 'text'
}, {
  name: 'company_name',
  title: 'Company',
  type: 'text'
}, {
  name: 'general',
  title: 'General'
}, {
  name: 'status',
  title: 'Status',
  type: 'boolean'
}]

export default class Reviews extends BaseDataTable {
  constructor(props) {
    super({
      props,
      Service: reviewService,
      LIST_COLUMNS,
      DETAIL_COLUMNS
    })
  }
}
