import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap'
import { Form, PaginatedTable } from '../components'
import * as utils from '../utils'

class DeleteModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      modal: false
    }
    this.toggle = this.toggle.bind(this)
    this.onConfirm = this.onConfirm.bind(this)
  }

  toggle(data) {
    this.setState({
      modal: !this.state.modal,
      data
    })
  }

  onConfirm() {
    this.props.deleteItem(this.state.data)
  }

  render() {
    return (<Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
      <ModalHeader toggle={this.toggle}>Xác nhận</ModalHeader>
      <ModalBody>
        Bạn có chắc chắn là mình muốn xóa?
      </ModalBody>
      <ModalFooter>
        <Button color="danger" onClick={this.onConfirm}>Xác nhận</Button>{' '}
        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
      </ModalFooter>
    </Modal>)
  }
}

export default class BaseDataTable extends Component {
  constructor(options) {
    const { props, Service, LIST_COLUMNS, DETAIL_COLUMNS, deleteEnabled } = options
    super(props)
    this.state = {
      Service,
      LIST_COLUMNS,
      DETAIL_COLUMNS,
      currentPage: null,
      nextPage: null,
      objects: [],
      prevPage: null,
      deleteEnabled
    }

    this.toggleModal = this.toggleModal.bind(this)
  }

  static contextTypes = {
    router: PropTypes.object
  }

  componentDidMount() {
    this._loadItems()
  }

  toggleModal(data) {
    this.refs.deleteModal.toggle(data)
  }

  render() {
    const { objects, LIST_COLUMNS, deleteEnabled, currentPage } = this.state
    return (
      <div className="animated fadeIn">
        <PaginatedTable ref="table"
          columns={LIST_COLUMNS}
          data={objects}
          deleteEnabled={deleteEnabled}
          page={currentPage}
          renderCreateLayout={this._renderCreateLayout}
          renderEditLayout={this._renderEditLayout}
          onNextPageClick={this._onNextPageClick}
          onOrderClick={this._onOrderClick}
          onPageSizeClick={this._onPageSizeClick}
          onPrevPageClick={this._onPrevPageClick}
          onSearchClick={this._onSearchClick} 
          toggleModal={this.toggleModal}
        />
        <DeleteModal ref="deleteModal" deleteItem={this._onDeleteItem} />
      </div>
    )
  }

  _loadItems = async (page = this.state.currentPage || 1) => {
    const { Service, limit = 10, search } = this.state
    try {
      const { currentPage, nextPage, objects: { count, rows }, prevPage } = await Service.getList(page, limit, search)
      this.setState({ currentPage, nextPage, objects: rows, count, prevPage })
    } catch (error) {
      
    }
  }

  _onCreateSubmit = async data => {
    const { Service } = this.state
    await Service.create(data).catch(error => {
      throw error
    })
    await this._loadItems()
    if (this.refs.createForm) {
      this.refs.createForm.reset()
    }
  }

  _onEditSubmit = async data => {
    const { Service } = this.state
    await Service.update(data).catch(error => {
      throw error
    })
    await this._loadItems()
    const newItem = await Service.getItem(data.id)
    if (this.refs.editForm) {
      this.refs.editForm.reset(newItem)
    }
  }

  _onNextPageClick = () => {
    if (this.state.nextPage) {
      this._loadItems(this.state.nextPage)
    }
  }

  _onOrderClick = (text) => {
    this._loadItems(utils.buildQuery(this.state.currentPage, {
      'order': text
    }))
  }

  _onPageSizeClick = (text) => {
    this.setState({
      limit: text
    })
    this._loadItems(this.state.currentPage)
  }

  _onPrevPageClick = () => {
    if (this.state.prevPage) {
      this._loadItems(this.state.prevPage)
    }
  }

  _onSearchClick = (text) => {
    this.setState({
      search: text
    })
    this._loadItems(this.state.currentPage)
  }

  _onDeleteItem = async data => {
    const { Service } = this.state
    try {
      await Service.deleteItem(data.id)
      this.toggleModal(null)
      
      this._loadItems(this.state.currentPage)
    } catch (error) {
      // console.log(error)
    }
    
  }

  _renderCreateLayout = () => {
    const { DETAIL_COLUMNS } = this.state
    return (
      <Form ref={(target) => this.refs.createForm = target}
        columns={DETAIL_COLUMNS}
        title="Create"
        onSubmit={this._onCreateSubmit} />
    )
  }

  _renderEditLayout = (object) => {
    const { DETAIL_COLUMNS } = this.state
    return (
      <Form ref={(target) => this.refs.editForm = target}
        columns={DETAIL_COLUMNS}
        data={object}
        title={`Edit - <${object.id}>`}
        onSubmit={this._onEditSubmit} />
    )
  }
}
