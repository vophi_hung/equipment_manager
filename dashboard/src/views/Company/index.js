import React, { Component } from 'react'
import { companyService } from '../../modules/ApiModule'
import BaseDataTable from '../BaseDataTable'

const DETAIL_COLUMNS = [{
  name: 'logo',
  title: 'Logo'
}, {
  name: 'name',
  title: 'Name'
}, {
  name: 'address',
  title: 'Address'
}]

const LIST_COLUMNS = [{
  name: 'id',
  title: 'ID',
  type: 'text'
}, {
  name: 'logo',
  title: 'Logo',
  type: 'image'
}, {
  name: 'name',
  title: 'Name'
}]

export default class Company extends BaseDataTable {

  constructor(props) {
    super({
      props,
      Service: companyService,
      LIST_COLUMNS,
      DETAIL_COLUMNS,
      deleteEnabled: true
    })
  }
}
