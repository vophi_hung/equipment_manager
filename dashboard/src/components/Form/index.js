import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Loader from 'halogen/PulseLoader'

import { Types } from '../../models/Base'
import * as utils from '../../utils'

export default class Form extends Component {

  static propTypes = {
    columns: PropTypes.array,
    data: PropTypes.object,
    model: PropTypes.any,
    title: PropTypes.string.isRequired,
    onSubmit: PropTypes.func
  }

  static defaultProps = {
    onSubmit: (_data) => { }
  }

  columnKeys = []
  data = null

  state = {
    errorMessage: null,
    inputChanged: false,
    submitting: false
  }

  componentDidMount() {
    this.componentWillReceiveProps(this.props)
    this.reset()
  }

  componentWillReceiveProps(nextProps) {
    this.columnKeys = nextProps.columns.map(column => column.name)
    this.data = nextProps.data ? nextProps.data : null
  }

  componentWillUpdate(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.reset()
    }
  }

  render() {
    return (
      <div className="component-form">
        <form ref="form" onSubmit={this._onSubmitClick}>
          <div className="card-header">
            <strong>{this.props.title.toUpperCase()}</strong>
            <span>&nbsp;{this.state.inputChanged && '(*)'}</span>
          </div>
          {this.state.errorMessage && (
            <div style={{ marginBottom: 0 }} className="card card-inverse card-danger text-center">
              <div style={{ textAlign: 'start' }} className="card-block">
                <blockquote className="card-blockquote">{this.state.errorMessage}</blockquote>
              </div>
            </div>
          )}
          <div className="card-block">
            {this.props.columns.map(this._renderColumn)}
          </div>
          <div className="card-footer">
            <button type="submit" className={`btn btn-sm ${this.state.inputChanged ? 'btn-primary' : 'btn-secondary'}`} disabled={!this.state.inputChanged}><i className="fa fa-dot-circle-o" /> Submit</button>
            <button style={{ marginLeft: '12px' }} type="button" className="btn btn-sm btn-secondary" disabled={!this.state.inputChanged} onClick={this._onResetClick}><i className="fa fa-ban" /> Reset</button>
          </div>
        </form>
        {this.state.submitting && <div className="processing-overlay"><Loader color="#ffffff" size="16px" margin="4px" /></div>}
      </div>
    )
  }

  reset = (data = this.data) => {
    for (let i = 0; i < this.refs.form.elements.length; i++) {
      const element = this.refs.form.elements[i]
      const { name, options, type } = element
      const value = utils.idx(data, data => data[name])
      switch (type) {
        case 'checkbox':
          this.refs.form.elements[i].checked = !!value
          break
        case 'select-one': {
          let selectedIndex = 0
          for (let i = 0; i < options.length; i++) {
            if (options[i].text === value) {
              selectedIndex = i
            }
          }
          element.selectedIndex = selectedIndex
          break
        }
        case 'number': {
          this.refs.form.elements[i].value = value || 0
          break          
        }
        default:
          this.refs.form.elements[i].value = value || ''
          break
      }
    }
    this.setState({ inputChanged: false })
  }

  submit = async () => {
    if (!this.state.inputChanged || this.state.submitting) {
      return
    }
    this.setState({ errorMessage: null, submitting: true })
    const id = utils.idx(this.data, data => data.id)
    const data = { id }

    for (let i = 0; i < this.refs.form.elements.length; i++) {
      const element = this.refs.form.elements[i]
      const { checked, name, options, selectedIndex, type, value } = element
      
      switch (type) {
        case 'checkbox':
          data[name] = checked
          break
        case 'select-one':
          data[name] = options[selectedIndex].value
          break
        default:
          if (value !== '') {
            data[name] = value
          }
          break
      }
    }
    await this.props.onSubmit(data).catch(error => {
      this.setState({ errorMessage: error.message })
    })
    this.setState({ submitting: false })
  }

  _onInputChange = () => {
    this.setState({ inputChanged: true })
  }

  _onResetClick = () => {
    this.reset()
  }

  _onSubmitClick = (e) => {
    e.preventDefault()
    this.submit()
  }

  _renderColumn = ({ name, title, type = 'text', placeholder = '...', rows = 8, disabled = false }) => {
    if (type == 'boolean') {
      return (
        <div key={name} className="form-group">
          <label htmlFor={name}>{title}</label>
          <div>
            <label className="switch switch-3d switch-primary">
              <input type="checkbox" className="switch-input" name={name} onChange={this._onInputChange} disabled={disabled} />
              <span className="switch-label" />
              <span className="switch-handle" />
            </label>
          </div>
        </div>
      )
    } else if (type == 'text') {
      return (
        <div key={name} className="form-group">
          <label htmlFor={name}>{title}</label>
          <input type="text" className="form-control" name={name} placeholder={placeholder} onChange={this._onInputChange}  disabled={disabled} />
        </div>
      )
    } else if (type === 'datetime') {
      return (
        <div key={name} className="form-group">
          <label htmlFor={name}>{title}</label>
          <input type="text" className="form-control" name={name} placeholder={placeholder} onChange={this._onInputChange} disabled={disabled} />
        </div>
      )
    }
    //  else if (Types.enum.is(type)) {
    //   return (
    //     <div key={name} className="form-group">
    //       <label htmlFor={name}>{Model.COLUMNS[name].title}</label>
    //       <select className="form-control" name={name} onChange={this._onInputChange}>
    //         {Model.COLUMNS[name].type.alias.map((alias, index) => {
    //           return <option key={`${name}-${index}`} value={Model.COLUMNS[name].type.values[index]}>{alias}</option>
    //         })}
    //       </select>
    //     </div>
    //   )
    // } else if (Types.number.is(type)) {
    //   return (
    //     <div key={name} className="form-group">
    //       <label htmlFor={name}>{Model.COLUMNS[name].title}</label>
    //       <input type="number" className="form-control" name={name} onChange={this._onInputChange} step="any" />
    //     </div>
    //   )
    // } else if (Types.string.is(type)) {
    //   return (
    //     <div key={name} className="form-group">
    //       <label htmlFor={name}>{Model.COLUMNS[name].title}</label>
    //       <textarea className="form-control" name={name} placeholder={placeholder} rows={rows} onChange={this._onInputChange} />
    //     </div>
    //   )
    // } else {
    //   return null
    // }
  }
}
