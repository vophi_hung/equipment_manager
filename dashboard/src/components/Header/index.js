import React, { Component } from 'react'
import { Dropdown, DropdownMenu, DropdownItem } from 'reactstrap'
import PropTypes from 'prop-types'

import { localStorageModule } from '../../modules'

export default class Header extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this)
    this.state = {
      dropdownOpen: false
    }
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  sidebarToggle(e) {
    e.preventDefault()
    document.body.classList.toggle('sidebar-hidden')
  }

  sidebarMinimize(e) {
    e.preventDefault()
    document.body.classList.toggle('sidebar-minimized')
  }

  mobileSidebarToggle(e) {
    e.preventDefault()
    document.body.classList.toggle('sidebar-mobile-show')
  }

  asideToggle(e) {
    e.preventDefault()
    document.body.classList.toggle('aside-menu-hidden')
  }

  render() {
    return (
      <header className="app-header navbar">
        <button className="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button" onClick={this.mobileSidebarToggle}>&#9776;</button>
        <a style={{ backgroundImage: 'none', textAlign: 'center' }} className="navbar-brand">
          <img style={{ height: '100%' }} alt="" src="./logo_symbol.png" />
        </a>
        <ul className="nav navbar-nav d-md-down-none">
          <li className="nav-item">
            <button className="nav-link navbar-toggler sidebar-toggler" type="button" onClick={this.sidebarToggle}>&#9776;</button>
          </li>
          <li className="nav-item px-3">
            <a className="nav-link" href="/">Dashboard</a>
          </li>
        </ul>
        <ul className="nav navbar-nav ml-auto">
          <li style={{ paddingRight: '20px' }} className="nav-item">
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <button onClick={this.toggle} className="nav-link dropdown-toggle" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded={this.state.dropdownOpen}>
                <img src={'./logo_symbol.png'} className="img-avatar" alt="admin@bootstrapmaster.com" />
                <span className="d-md-down-none">admin</span>
              </button>

              <DropdownMenu className="dropdown-menu-right">
                <DropdownItem onClick={this._onLogoutClick}><i className="fa fa-lock" /> Logout</DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </li>
        </ul>
      </header>
    )
  }

  _onLogoutClick = () => {
    localStorageModule.setToken(null)
    this.context.router.history.replace('/login')
  }
}
