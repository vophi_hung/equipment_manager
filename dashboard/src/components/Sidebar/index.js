import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault()
    e.target.parentElement.classList.toggle('open')
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown'
  }

  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse"
  // }

  render() {
    return (
      <div className="sidebar">
        <nav className="sidebar-nav">
          <ul className="nav">
            <li className="nav-item">
              <NavLink to={'/'} className="nav-link" activeClassName="active"><i className="icon-speedometer" /> Dashboard <span className="badge badge-info">NEW</span></NavLink>
            </li>
            <li className="nav-title">
              Tables
            </li>
            <li className="nav-item">
              <NavLink to={'/tables/company'} className="nav-link" activeClassName="active"><i className="icon-tag" /> Company</NavLink>
            </li>
            
            <li className="nav-item">
              <NavLink to={'/tables/review'} className="nav-link" activeClassName="active"><i className="icon-tag" /> Review</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/tables/user'} className="nav-link" activeClassName="active"><i className="icon-tag" /> User</NavLink>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}
