import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <a href="http://daitiengnoinhanvien.com">Dai tieng noi nhan vien</a> &copy; 2017
      </footer>
    )
  }
}
