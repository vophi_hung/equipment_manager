import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Types } from '../../models/Base'

export default class PaginatedTable extends Component {

  static propTypes = {
    columns: PropTypes.array,
    createEnabled: PropTypes.bool,
    data: PropTypes.array,
    editEnabled: PropTypes.bool,
    model: PropTypes.any,
    page: PropTypes.any,
    renderCreateLayout: PropTypes.func,
    renderEditLayout: PropTypes.func,
    onDeleteClick: PropTypes.func,
    onCreateClick: PropTypes.func,
    onEditClick: PropTypes.func,
    onNextPageClick: PropTypes.func,
    onOrderClick: PropTypes.func,
    onPageSizeClick: PropTypes.func,
    onPrevPageClick: PropTypes.func,
    onSearchClick: PropTypes.func
  }

  static defaultProps = {
    createEnabled: true,
    editEnabled: true,
    deleteEnabled: false,
    onDeleteClick: () => { },
    onCreateClick: () => { },
    onEditClick: (_object) => { },
    onNextPageClick: () => { },
    onOrderClick: (_text) => { },
    onPageSizeClick: (_text) => { },
    onPrevPageClick: () => { },
    onSearchClick: (_text) => { }
  }

  state = {
    object: null,
    order: '',
    overlayMode: null,
    pageSize: 20,
    search: ''
  }

  componentWillUnmount() {
    this._onDimissOverlay()
  }

  render() {
    const { columns, createEnabled, data, deleteEnabled, editEnabled } = this.props
    return (
      <div className="card component-paginated-table">
        <div className="overlay">
          <div className="panel">
            <div className="panel-header">
              <button type="button" className="btn btn-link btn-icon-circle" onClick={this._onDimissOverlay}><i className="fa fa-close" /></button>
            </div>
            <div className="panel-body">
              {this._renderOverlay()}
            </div>
          </div>
        </div>
        {createEnabled && (
          <div className="card-header">
            <button style={{ marginTop: 'auto' }} type="button" className="btn btn-primary" onClick={this._onCreateClick}><i className="fa fa-plus" /> Create</button>
          </div>
        )}
        <div className="card-block">
          <div className="paginated-table-toolbar">
            <div className="input-group search-bar">
              <input className="form-control" size="16" type="text" name="search" placeholder={'Text'} onChange={this._onInputChange} />
              <span className="input-group-btn"><button className="btn btn-default" type="button" onClick={this._onSearchClick}>Search</button></span>
            </div>
            <div className="input-group order-bar">
              <input className="form-control" size="16" type="text" name="order" placeholder={'Ex: [["updated_at", "desc"]]'} onChange={this._onInputChange} />
              <span className="input-group-btn"><button className="btn btn-default" type="button" onClick={this._onOrderClick}>Order</button></span>
            </div>
            <div className="input-group page-size-bar">
              <input className="form-control" size="16" type="text" name="page-size" placeholder={'Ex:20'} onChange={this._onInputChange} />
              <span className="input-group-btn"><button className="btn btn-default" type="button" onClick={this._onPageSizeClick}>PageSize</button></span>
            </div>
            <ul className="pagination">
              <li className="page-item"><a className="page-link" onClick={this.props.onPrevPageClick}>Prev</a></li>
              <li style={{ minWidth: '48px', textAlign: 'center' }} className="page-item active">
                <a className="page-link">{this._getPageNumber()}</a>
              </li>
              <li className="page-item"><a className="page-link" onClick={this.props.onNextPageClick}>Next</a></li>
            </ul>
          </div>
          <div className="card-table">
            <table className="table">
              <thead>
                <tr>
                  {deleteEnabled && <th style={{ width: '48px' }} />}
                  {editEnabled && <th style={{ width: '48px' }} />}
                  {columns.map(column => {
                    const percentage = column.percentage ? `${column.percentage}%` : 'auto'
                    return (
                      <th key={column.name} style={{ width: percentage }}>{column.title}</th>
                    )
                  })}
                </tr>
              </thead>
              <tbody>
                {data.map(object => {
                  return (
                    <tr key={object.id}>
                      {editEnabled && <td><button type="button" className="btn btn-primary btn-icon-circle btn-sm" onClick={() => this._onEditClick(object)}><i className="fa fa-info" /></button></td>}
                      {deleteEnabled && <td><button type="button" className="btn btn-danger btn-icon-circle btn-sm" onClick={() => this._onDeleteClick(object)}><i className="fa fa-times-circle-o" /></button></td>}
                      {columns.map(column => {
                        return (
                          <td key={column.name}>{this._renderCell(object[column.name], column)}</td>
                        )
                      })}
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  dismissOverlay = () => {
    this._onDimissOverlay()
  }

  _getPageNumber = () => {
    let page = this.props.page
    try {
      page = page.split('&').reduce((acc, p) => {
        p = p.split('=')
        return p[0] === 'page' ? p[1] : acc
      }, null)
    } catch (ignore) {
    }
    return page || 1
  }

  _onDimissOverlay = () => {
    this._overlayEnabled(false)
    this.setState({ object: null, overlayMode: null })
  }

  _onCreateClick = () => {
    if (this.props.renderCreateLayout) {
      this._overlayEnabled(true)
      this.setState({ object: null, overlayMode: 'create' })
    } else {
      this.props.onCreateClick()
    }
  }

  _onDeleteClick = async (data) => {
    if (this.props.onDeleteClick) {
      this.props.toggleModal(data)
      // await this.props.onDeleteClick(data)
    }
  }

  _onEditClick = (object) => {
    if (this.props.renderEditLayout) {
      this._overlayEnabled(true)
      this.setState({ object, overlayMode: 'edit' })
    } else {
      this.props.onEditClick(object)
    }
  }

  _onInputChange = (e) => {
    const value = e.target.value
    switch (e.target.name) {
      case 'order':
        this.setState({ order: value })
        break
      case 'search':
        this.setState({ search: value })
        break
      case 'page-size':
        this.setState({ pageSize: value })
        break
      default:
        break
    }
  }

  _onOrderClick = () => {
    this.props.onOrderClick(this.state.order)
  }

  _onPageSizeClick = () => {
    this.props.onPageSizeClick(this.state.pageSize)
  }

  _onSearchClick = () => {
    this.props.onSearchClick(this.state.search)
  }

  _overlayEnabled = (enabled) => {
    const key = 'component-paginated-table-shown'
    const contained = document.body.classList.contains(key)
    if (enabled && !contained) {
      document.body.classList.add(key)
    } else if (!enabled && contained) {
      document.body.classList.remove(key)
    }
  }

  _renderCell = (value, column) => {
    // const { type } = this.props.model.COLUMNS[column.name]
    const { type, custom } = column

    if (type === 'boolean') {
      if (value) {
        return <div style={{ ...column.style || {} }}><span className="badge badge-success">true</span></div>
      } else {
        return <div style={{ ...column.style || {} }}><span className="badge badge-default">false</span></div>
      }
    } else if (type === 'image') {
      return <img src={value} style={{
        maxHeight: 30,
        width: 'auto'
      }} />
    } else if( type === 'custom') {
      return custom(value)
    } else {
      return (
        <div style={{ ...column.style || {} }} title={value}>{value}</div>
      )
    }
  }

  _renderOverlay = () => {
    switch (this.state.overlayMode) {
      case 'create':
        return this.props.renderCreateLayout()
      case 'edit':
        return this.props.renderEditLayout(this.state.object)
      default:
        return null
    }
  }
}
