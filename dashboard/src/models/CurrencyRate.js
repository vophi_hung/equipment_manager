import Base, { Types } from './Base'

export default class CallRate extends Base {

  static COLUMNS = {
    'id': {
      title: 'ID'
    },
    'active': {
      title: 'Actice',
      type: Types.bool
    },
    'code': {
      title: 'Code'
    },
    'currency': {
      title: 'Currency'
    },
    'format': {
      title: 'Format'
    },
    'order': {
      title: 'Order',
      type: Types.number
    },
    'rate': {
      title: 'Rate'
    },
    'visible': {
      title: 'Visible',
      type: Types.bool
    }
  }
}
