import Base, { Types } from './Base'

export default class Product extends Base {

  static COLUMNS = {
    'id': {
      title: 'ID'
    },
    'active': {
      title: 'Active',
      type: Types.bool
    },
    'description': {
      title: 'Description',
      type: Types.string
    },
    'expire_at': {
      title: 'ExpireAt',
      type: Types.datetime
    },
    'expire_in': {
      title: 'ExpireIn',
      type: Types.number
    },
    'gp_product_code': {
      title: 'GPProductCode'
    },
    'icon_url': {
      title: 'IconURL'
    },
    'order': {
      title: 'Order',
      type: Types.number
    },
    'price': {
      title: 'Price',
      type: Types.number
    },
    'regions_code': {
      title: 'RegionsCode'
    },
    'seconds': {
      title: 'Seconds',
      type: Types.number
    },
    'title': {
      title: 'Title'
    },
    'visible': {
      title: 'Visible',
      type: Types.bool
    }
  }
}
