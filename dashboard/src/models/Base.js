import _ from 'lodash'
import * as utils from '../utils'

const _bool = () => {
  return {
    getter: value => !!value,
    setter: value => !!value
  }
}

const _char = () => {
  return {
    getter: value => `${value || ''}`,
    setter: value => `${value || ''}`
  }
}

const _datetime = () => {
  return {
    getter: value => value ? new Date(value) : undefined,
    setter: value => value ? new Date(value) : undefined
  }
}

const _enum = (values = [], options = {}) => {
  let { alias } = options
  values = values.map(value => `${value}`)
  alias = alias || values
  return {
    alias,
    getter: value => {
      return alias[values.indexOf(`${value}`)]
    },
    setter: value => {
      return values[alias.indexOf(`${value}`)]
    },
    values
  }
}

const _number = () => {
  return {
    getter: value => Number(value),
    setter: value => Number(value)
  }
}

const _string = () => {
  return {
    getter: value => `${value || ''}`,
    setter: value => `${value || ''}`
  }
}

export const Types = {
  bool: _bool,
  char: _char,
  datetime: _datetime,
  enum: _enum,
  number: _number,
  string: _string
}

Object.keys(Types).forEach(key => {
  const func = Types[key]
  const wrapper = (...args) => Object.assign({ __type: key }, func(...args))
  wrapper.is = (target) => target && (target === wrapper || target.__type === key)
  Types[key] = wrapper
})

export default class Base {

  static COLUMNS = {}

  constructor(data = {}) {
    this._data = data
    Object.keys(this.constructor.COLUMNS).forEach(key => {
      this.constructor.COLUMNS[key].type = this.constructor.COLUMNS[key].type || Types.char
      if (typeof this.constructor.COLUMNS[key].type === 'function') {
        this.constructor.COLUMNS[key].type = this.constructor.COLUMNS[key].type()
      }
      key = _.camelCase(key)
      if (!this.constructor.prototype.hasOwnProperty(key)) {
        Object.defineProperty(this.constructor.prototype, key, {
          get: function () {
            return this.get(key)
          },
          set: function (value) {
            this.set(key, value)
          }
        })
      }
    })
  }

  clone() {
    const Clazz = this.constructor
    return new Clazz(Object.assign({}, this._data))
  }

  get(key) {
    key = _.snakeCase(key)
    if (!this.constructor.COLUMNS[key]) {
      return undefined
    }
    return this.constructor.COLUMNS[key].type.getter(this._data[key])
  }

  merge(data) {
    data = data || {}
    data = data.toJSON ? data.toJSON() : data
    utils.merge(this._data, data)
  }

  set(key, value) {
    key = _.snakeCase(key)
    if (!this.constructor.COLUMNS[key]) {
      return
    }
    this._data[key] = this.constructor.COLUMNS[key].type.setter(value)
  }

  toJSON() {
    return this._data
  }
}
