import AdminService from './AdminService'
import CompanyService from './CompanyService'
import ReviewService from './ReviewService'
import UserService from './UserService'

export const
  adminService = new AdminService(),
  companyService = new CompanyService(),
  reviewService = new ReviewService(),
  userService = new UserService()
