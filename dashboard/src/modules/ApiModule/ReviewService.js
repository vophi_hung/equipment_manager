import CrudService from './CrudService'

export default class CompanyService extends CrudService {
  constructor() {
    super()
    this.uri = 'review'
    this.DEFAULT_PAGE = 'order=%5B%5B%22active%22%2C%22desc%22%5D%2C%5B%22role%22%2C%22desc%22%5D%2C%5B%22email%22%2C%22asc%22%5D%5D'
  }
}