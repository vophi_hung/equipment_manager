import CrudService from './CrudService'
import { Admin } from '../../models'

export default class AdminService extends CrudService {
  constructor() {
    super(Admin)
    this.uri = 'admins'
  }

  login(accesstoken) {
    return this.post('/api/v1/user/login/facebook', { access_token: accesstoken })
  }
}