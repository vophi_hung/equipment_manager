import HttpService from './HttpService'

export default class CrudService extends HttpService {

  constructor(Model) {
    super()
    this.Model = Model
  }

  //API
  async create(item) {
    return await this.post(`/api/v1/${this.uri}`, item)
  }

  async deleteItem(id) {
    return await this.delete(`/api/v1/${this.uri}/${id}`)
  }

  async getItem(id) {
    return this.get(`/api/v1/${this.uri}/${id}`)
  }

  async getList(page = 1, limit = 10, search = '') {
    const result = await this.get(`/api/v1/${this.uri}?offset=${(page - 1) * limit}&limit=${limit}&search=${search}`)
    // const result = await this.get(`/api/v1/${this.uri}?${page || this.DEFAULT_PAGE}`)
    return result
  }

  async update(item) {
    return await this.put(`/api/v1/${this.uri}/${item.id}`, item)
  }
}