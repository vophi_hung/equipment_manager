const concat = require('gulp-concat')
const fs = require('fs')
const gulp = require('gulp')
const path = require('path')
const runSequence = require('run-sequence')
const sass = require('gulp-sass')

const isArray = (items) => {
  return Array.isArray(items)
}

const isObject = (item) => {
  return (item && typeof item === 'object' && !isArray(item))
}

const merge = (...args) => {
  const target = args[0]
  args.filter((value, key) => key > 0).forEach(value => _mergeAPair(target, value))
  return target
}

const _mergeAPair = (target, source) => {
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(key => {
      if (isObject(source[key])) {
        if (!target[key]) {
          Object.assign(target, { [key]: {} })
        }
        _mergeAPair(target[key], source[key])
      } else {
        Object.assign(target, { [key]: source[key] })
      }
    })
  }
  return target
}

gulp.task('build', function (callback) {
  runSequence('config', 'sass', callback)
})

gulp.task('config', function (callback) {
  const defaultConfig = require('./config/default')
  const envConfig = require(`./config/${process.env.NODE_ENV || 'local'}`)
  const config = merge({}, defaultConfig, envConfig)
  fs.writeFileSync(path.resolve(__dirname, './src/config.json'), JSON.stringify(config))
  callback()
})

gulp.task('sass', function () {
  return gulp.src('./scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./public/css'))
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('./public/css'))
})

gulp.task('watch', function () {
  gulp.watch('./config/**/*.js', ['config'])
  gulp.watch('./scss/**/*.scss', ['sass'])
})

gulp.task('default', function (callback) {
  runSequence('build', 'watch', callback)
})
