module.exports = {
  env: 'NODE_ENV',
  debug: false,

  apiEndpoint: 'https://SERVER_URL',
  googleClientId: 'GOOGLE_CLIENT_ID'
}
