import CONST from '../const'
import {
  sequelize,
  Sequelize
} from './base'

export default sequelize.define('tbl_action', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  user_id: Sequelize.STRING,
  company_id: Sequelize.STRING,
  review_id: Sequelize.STRING,
  action: Sequelize.STRING,
  created_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  },
  updated_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  }
}, {
  timestamps: true,
  underscored: true,
  freezeTableName: true
})