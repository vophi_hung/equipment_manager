import {
  sequelize,
  Sequelize
} from './base'

export default sequelize.define('tbl_review', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  user_id: Sequelize.STRING,
  company_id: Sequelize.STRING,
  general: Sequelize.TEXT,
  cons: Sequelize.TEXT,
  pros: Sequelize.TEXT,
  mess_to_manager: Sequelize.TEXT,
  rating: Sequelize.INTEGER,
  status: Sequelize.BOOLEAN,
  created_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  },
  updated_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  }
}, {
  freezeTableName: true,
  timestamps: true,
  underscored: true
})