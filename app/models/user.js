import {
  sequelize,
  Sequelize
} from './base'

export default sequelize.define('tbl_user', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  avatar: Sequelize.STRING,
  name: Sequelize.STRING,
  social_id: Sequelize.STRING,
  type: Sequelize.STRING,
  created_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  },
  updated_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  }
}, {
  freezeTableName: true,
  timestamps: true,
  underscored: true
})