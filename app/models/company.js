import {
  sequelize,
  Sequelize
} from './base'

export default sequelize.define('tbl_company', {
  id: {
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true,
    type: Sequelize.UUID
  },
  address: Sequelize.STRING,
  logo: Sequelize.STRING,
  name: Sequelize.STRING,
  type: Sequelize.STRING,
  created_at: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: 'TIMESTAMP' 
  },
  updated_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  }
}, {
  freezeTableName: true,
  timestamps: true,
  underscored: true
})