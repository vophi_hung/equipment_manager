import { Router } from 'express'
import BaseRouter from './base'
import {
  pageInfoMiddleware,
  extractSessionMiddleware
} from '@/middlewares'

export default class CrudRouter extends BaseRouter {
  constructor(Controller, useDefault = true, auth) {
    super()
    this.Controller = Controller
    this.router = Router()
    useDefault && this._defaultRouter(auth)
  }

  _defaultRouter(auth) {
    if( auth && auth.auth ) {
      this.router.use([extractSessionMiddleware.run(auth.must, auth.role)])
    }
    this.router.get('/', [pageInfoMiddleware.run()], this.route(this.getList))
    this.router.get('/:id', this.route(this.getItem))
    this.router.post('/', this.route(this.create))
    this.router.put('/:id', this.route(this.update))
    this.router.delete('/:id', this.route(this.delete))
  }

  async create(req, res) {
    res.item = await this.Controller.create(req.body)
    this.onSuccess(res, res.item)
  }

  async delete(req, res) {
    await this.Controller.delete(req.params)
    this.onSuccess(res)
  }
  
  async getItem(req, res) {
    req.item = await this.Controller.getItem(req.params)
    this.onSuccess(res, req.item)
  }

  async getList(req, res) {
    req.items = await this.Controller.getList({}, Object.assign(req.pageInfo, req.auth))
    this.onSuccessAsList(res, req.items, undefined, req.pageInfo)
  }
  
  async update(req, res) {
    req.body = Object.assign(req.body, req.params)
    res.item = await this.Controller.update(req.body)
    this.onSuccess(res, res.item)
  }
}
