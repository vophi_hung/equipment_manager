import CrudRouter from '../crud'
import {
  categoryController
} from '@/controllers'

export default class CategoryRouter extends CrudRouter {
  constructor() {
    super(categoryController)
  }
}