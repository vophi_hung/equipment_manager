import CrudRouter from '../crud'
import {
  userController
} from '@/controllers'
import {
  pageInfoMiddleware
} from '@/middlewares'

export default class UserRouter extends CrudRouter {
  constructor() {
    super(userController)
    this.router.post('/login/facebook', this.route(this.loginFacebook))
  }

  async loginFacebook(req, res) {
    const data = await this.Controller.login(req, res)        
    this.onSuccess(res, data)
  }
}
