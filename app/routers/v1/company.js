import CrudRouter from '../crud'
import {
  companyController
} from '@/controllers'

export default class CompanyRouter extends CrudRouter {
  constructor() {
    super(companyController, false, {
      auth: true,
      must: false,
      role: [
        'admin'
      ]
    })
    
    this.router.get('/:id', this.route(super.getItem))
    this.router.get('/search/:keyword', this.route(this.search))
    super._defaultRouter({
      auth: true,
      must: false,
      role: [
        'admin'
      ]
    })
  }

  async search(req, res) {
    this.onSuccessAsList(
      res, 
      await this.Controller.search(req.params.keyword), 
      undefined, 
      req.pageInfo)
  }
}
