import CrudRouter from '../crud'
import {
  reviewController
} from '@/controllers'
import {
  extractSessionMiddleware,
  pageInfoMiddleware
} from '@/middlewares'

export default class ReviewRouter extends CrudRouter {
  constructor() {
    super(reviewController, true, {
      auth: true,
      must: false,
      role: [
        'admin'
      ]
    })
    this.router.get('/company/all', [extractSessionMiddleware.run(false), pageInfoMiddleware.run()], this.route(this.all))
    this.router.post('/company/:id', [extractSessionMiddleware.run()], this.route(this.create))
    this.router.get('/company/:id', [pageInfoMiddleware.run(false)], this.route(this.getcompanyReview))
  }

  async all(req, res) {
    const { auth = {}, pageInfo } = req
    try {
      req.items = await this.Controller.all({
        status: true
      }, Object.assign(auth, pageInfo))
    } catch (error) {
      console.log(error)
    }
    this.onSuccessAsList(res, req.items, undefined, req.pageInfo)
  }

  async create(req, res) {
    req.body.company_id = req.params.id
    req.body.user_id = req.auth.id
    res.item = await this.Controller.create(req.body)
    this.onSuccess(res, res.item)
  }

  async getcompanyReview(req, res) {
    try {
      req.items = await this.Controller.getList({
        company_id: req.params.id,
        status: true
      }, req.pageInfo)
    } catch (error) {}
    this.onSuccessAsList(res, req.items, undefined, req.pageInfo)
  }
}