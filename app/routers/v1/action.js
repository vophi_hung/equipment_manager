import CrudRouter from '../crud'
import {
  actionController
} from '@/controllers'
import {
  extractSessionMiddleware
} from '@/middlewares'

export default class CompanyRouter extends CrudRouter {
  constructor() {
    super(actionController, false)
    this.router.get('/:review_id/like', [extractSessionMiddleware.run()], this.route(this.like))
    this.router.get('/:review_id/dislike', [extractSessionMiddleware.run()], this.route(this.dislike))
    this.router.get('/:review_id/status', [extractSessionMiddleware.run(false)], this.route(this.status))
    super._defaultRouter()
  }

  async like(req, res) {
    await this.Controller.like({
      review_id: req.params.review_id,
      user_id: req.auth.id
    })
    this.onSuccess(res)
  }

  async dislike(req, res) {
    await this.Controller.dislike({
      review_id: req.params.review_id,
      user_id: req.auth.id
    })
    this.onSuccess(res)
  }

  async status(req, res) {
    const condition = {
      review_id: req.params.review_id
    }

    if (req.auth) {
      condition.user_id = req.auth.id
    }

    const status = await this.Controller.status(condition)
    this.onSuccess(res, status)
  }
}
