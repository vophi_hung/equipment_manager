import {
  actionService,
  reviewService,
} from '@/services'

import CrudController from './crudController'


export default class actionController extends CrudController {
  constructor() {
    super(actionService)
  }

  async like({ user_id, review_id }) {
    const exLike = await actionService.getItem({
      user_id,
      review_id,
      action: 'like'
    })

    if(exLike) {
      await actionService.delete({ id: exLike.id })
      return
    }
    
    const exDisLike = await actionService.getItem({
      user_id,
      review_id,
      action: 'dislike'
    })

    if(exDisLike) {
      await actionService.delete({ id: exDisLike.id })
    }

    const review = await reviewService.getItem({
      id: review_id
    })

    await actionService.create({
      user_id,
      company_id: review.company_id,
      review_id,
      action: 'like'
    })
  }

  async dislike({ user_id, review_id }) {
    const exDisLike = await actionService.getItem({
      user_id,
      review_id,
      action: 'dislike'
    })

    if(exDisLike) {
      await actionService.delete({ id: exDisLike.id })
      return
    }

    const exLike = await actionService.getItem({
      user_id,
      review_id,
      action: 'like'
    })

    if(exLike) {
      await actionService.delete({ id: exLike.id })
    }

    const review = await reviewService.getItem({
      id: review_id
    })

    await actionService.create({
      user_id,
      company_id: review.company_id,
      review_id,
      action: 'dislike'
    })
  }

  async status({ user_id, review_id }) {
    return {
      mine: await actionService.getItem({
        user_id,
        review_id,
      }),
      count: {
        like: await actionService.count({
          review_id,
          action: 'like'
        }),
        dislike: await actionService.count({
          review_id,
          action: 'dislike'
        })
      }
    }
  }
}