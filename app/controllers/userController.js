import {
  configService,
  userService,
  utilService
} from '@/services'
import CrudController from './crudController'
import request from 'request-promise'
import moment from 'moment'


export default class UserController extends CrudController {
  constructor() {
    super(userService)
  }

  async login(req, res) {
    //get from facebook
    let { id: social_id, name } = await request({
      method: 'GET',
      uri: 'https://graph.facebook.com/me',
      qs: { access_token: req.body.access_token},
      json: true
    })
    let user
    try {
      user = await this.Service.getItem({ social_id })
      
    } catch (error) {
      
    }
    if(!user) {
      user = await this.Service.create({
        social_id,
        name,
        avatar: `http://graph.facebook.com/${social_id}/picture?type=large`
      })
    }
    const current = moment().valueOf()
    const token = utilService.generateAuthToken({
      id: user.id,
      current: current,
      type: user.type
    }, configService.getSecret())
    return {
      token,
      current,
      type: user.type
    }
  }
}