import {
  companyService,
  reviewService,
  userService
} from '@/services'
import CrudController from './crudController'


export default class ReviewController extends CrudController {
  constructor() {
    super(reviewService)
  }

  async getList(params, options) {
    const { rows, count } = await this.Service.getList(...arguments)
    const tasks = []
    const { type } = options
    rows.forEach(row => {
      row = row.toJSON()
      tasks.push(Promise.all([
        companyService.getItem({
          id: row.company_id
        }),
        userService.getItem({
          id: row.user_id
        })
      ]).then( ([company, user]) => {
        row.company_name = company.name
        row.company_logo = company.logo
        if (type === 'admin') {
          row.user_name = user.name
        }
        return row
      }))
    })

    return {
      count,
      rows: await Promise.all(tasks)
    }
  }

  async all(query, pageInfo) {
    const { count, rows } = await this.Service.getList(...arguments)
    const tasks = []

    rows.forEach(row => {
      row = row.toJSON()
      tasks.push(companyService.getItem({
        id: row.company_id
      })
      .then(company => {
        row.company = company
        return row
      }))
    })


    return {
      count,
      rows: await Promise.all(tasks)
    }
  }
}