import {
  categoryService
} from '@/services'
import CrudController from './crudController'


export default class categoryController extends CrudController {
  constructor() {
    super(categoryService)
  }
}