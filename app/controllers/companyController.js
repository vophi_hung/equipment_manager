import {
  companyService
} from '@/services'
import CrudController from './crudController'


export default class companyController extends CrudController {
  constructor() {
    super(companyService)
  }

  async getItem(params) {
    let item = await this.Service.getItem(...arguments)
    return item
  }

  async search(keyword) {
    if (keyword === '')
      return []
    return await companyService.search(keyword)
  }
}