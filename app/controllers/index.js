import ActionController from './actionController'
import CompanyController from './companyController'
import CategoryController from './categoryController'
import ReviewController from './reviewController'
import UserController from './userController'

const actionController = new ActionController()
const companyController = new CompanyController()
const reviewController = new ReviewController()
const userController = new UserController()

export {
  actionController,
  companyController,
  reviewController,
  userController
}
