import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'
import logger from 'morgan'
import api from './routers'
import {
  configService
} from './services'

let app = express()

app.use(logger('common'))
  .use(bodyParser.json({
    limit: '50mb'
  }))
  .use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
  }))
  .use('/', [
    cors({
      origin: configService.getAllowOrigin(),
      optionsSuccessStatus: 200
    })
  ])
  .use('/api', api)
//  .use(response_helper)


app.listen(configService.getServerPort(), () => {
  console.log('%s started at %s://%s:%s',
    configService.getServerName(),
    configService.getServerProtocol(),
    configService.getServerHost(),
    configService.getServerPort()
  )
})


export default app;