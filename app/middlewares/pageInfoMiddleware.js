import CONST from '@/const'
import BaseMiddleware from './base'

export default class PageInfoMiddleware extends BaseMiddleware {
  use() {
    return (req, res, next) => {
      const filter = this._parseFilter(req)
      const order = this._parseOrder(req)
      const page = parseInt(req.query['page'] || 1)
      const limit = parseInt(req.query['limit'] || CONST.PAGE_SIZE)
      const offset = parseInt(req.query['offset']) || (page - 1) * limit
      const search = req.query['search'] || ''
      req.pageInfo = {
        filter,
        limit,
        offset,
        order,
        page,
        search
      }
      next()
    }
  }

  /**
   * Filter param only accept <and> query. <or> will be supported later
   * Format: [[key, operator, value], [key, operator, value]]
   */
  _parseFilter(req) {
    let filter = req.query['filter']
    try {
      filter = JSON.parse(filter)
    } catch (ignore) {
      filter = null
    }
    return filter || {}
  }

  /**
   * Format: [[key, order], [key, order]]
   */
  _parseOrder(req) {
    let order = req.query['order']
    try {
      order = JSON.parse(order)
    } catch (ignore) {
      order = null
    }
    return order || [
      ['updated_at', 'desc']
    ]
  }
}