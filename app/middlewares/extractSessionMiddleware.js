import CONST from '@/const'
import BaseMiddleware from './base'
import {
  configService,
  errorService,
  utilService
} from '@/services'

export default class PageInfoMiddleware extends BaseMiddleware {
  use(mustLogin = true, roleRequire = []) {
    return (req, res, next) => {
      try {
        const authorization = req.header('authorization').toLowerCase().replace('bearer', '').trim()
        const {
          id,
          type = ''
        } = utilService.verifyAuthToken(authorization, configService.getSecret())
        req.auth = {
          id,
          type
        }
        if (roleRequire.length > 0 && roleRequire.indexOf(type) === -1) {
          throw errorService.unauthorized()
        }
      } catch (error) {
        if(mustLogin) {
          throw error
        }
      }
      
      next()
    }
  }
}