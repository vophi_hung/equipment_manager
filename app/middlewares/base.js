export default class BaseMiddleware {
  run() {
    return this.use(...arguments)
  }
}