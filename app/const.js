module.exports = {
  STATUS_ENUM: [
    'active',
    'inactive',
    'deleted'
  ],

  PAGE_SIZE: 10
}