import Base from './base'

const Data = Base.model('data', {
  name: String,
  sensor: String,
  value: String,
  timeStamp: Date,
  value: Number,
  quality: String
})

export default Data