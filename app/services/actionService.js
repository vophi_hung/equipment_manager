import {
  Action
} from '@/models'
import CrudService from './crudService'

export default class ActionService extends CrudService {
  constructor() {
    super(Action)
  }
}