import crypto from 'crypto'
import { errorService } from './'

export default class UtilService {
  stringifyQuery(query) {
    query = query || {}
    query = Object.keys(query).reduce((acc, key) => {
      if (query[key] !== undefined && query[key] !== null) {
        acc.push(`${key}=${encodeURIComponent(typeof query[key] === 'object' ? JSON.stringify(query[key]) : query[key])}`)
      }
      return acc
    }, [])
    return query.join('&')
  }

  generateAuthToken(payload, secretKey) {
    var cipher = crypto.createCipher( 'aes-256-ctr', secretKey)
    var crypted = cipher.update(JSON.stringify(payload), 'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
  }

  verifyAuthToken(token, secretKey) {
    try {
      var decipher = crypto.createDecipher('aes-256-ctr',secretKey)
      var dec = decipher.update(token,'hex','utf8')
      dec += decipher.final('utf8');
      return JSON.parse(dec);
    } catch (e) {
      throw errorService.unauthorized()
    }
  }
}