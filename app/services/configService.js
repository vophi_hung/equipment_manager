import config from 'config'

export default class configService {
  getDbDatabase() {
    return config.get('db.database')
  }

  getDbUserName() {
    return config.get('db.username')
  }

  getDbPasword() {
    return config.get('db.name')
  }

  getDbHost() {
    return config.get('db.host')
  }

  getDbDialect() {
    return config.get('db.dialect')
  }


  getServerHost() {
    return config.get('server.host')
  }

  getServerName() {
    return config.get('server.name')
  }

  getServerPort() {
    return config.get('server.port')
  }

  getServerProtocol() {
    return config.get('server.protocol')
  }

  getSecret() {
    return config.get('secret')
  }

  getAllowOrigin() {
    return config.get('allowOrigin')
  }
}