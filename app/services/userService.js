import {
  User
} from '@/models'
import CrudService from './crudService'

export default class UserService extends CrudService {
  constructor() {
    super(User)
  }
}