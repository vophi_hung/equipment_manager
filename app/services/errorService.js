/* eslint prefer-rest-params: 0 */

class BaseError extends Error {

  constructor(code, type, message, data) {
    super()
    this.code = code
    this.type = type
    this.message = message
    this.data = data
  }

  toJSON() {
    return {
      code: this.code,
      type: this.type,
      message: this.message,
      data: this.data
    }
  }
}

class ExternalRequestError extends BaseError {

  constructor(data) {
    super(500, 'external_request_error', 'External request error', data)
  }
}

class RecordNotfound extends BaseError {
  constructor() {
    super(400, 'record_not_found', 'Record not found')
  }
}

class SomethingWentWrong extends BaseError {
  constructor() {
    super(500, 'something_went_wrong', 'Sorry! Something went wrong.')
  }
}

class Unauthorized extends BaseError {
  constructor() {
    super(401, 'unauthorized', 'Unauthorized')
  }
}

class UserHaveBeenBlocked extends BaseError {
  constructor() {
    super(400, 'user_have_been_blocked', 'User have been blocked')
  }
}

export default class ErrorService {

  externalRequestError() {
    return new ExternalRequestError(...arguments)
  }
  
  recordNotFound() {
    return new RecordNotfound(...arguments)
  }

  somethingWentWrong() {
    return new SomethingWentWrong(...arguments)
  }

  unauthorized() {
    return new Unauthorized(...arguments)
  }

  userHaveBeenBlocked() {
    return new UserHaveBeenBlocked(...arguments)
  }
}