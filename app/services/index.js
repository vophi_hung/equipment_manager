import ActionService from './actionService'
import CategoryService from './categoryService'
import CompanyService from './companyService'
import ConfigService from './configService'
import ErrorService from './errorService'
import ReviewService from './reviewService'
import UserService from './userService'
import UtilService from './utilService'

//Singleton
const actionService = new ActionService()
const companyService = new CompanyService()
const configService = new ConfigService()
const categoryService = new CategoryService()
const errorService = new ErrorService()
const reviewService = new ReviewService()
const utilService = new UtilService()
const userService = new UserService()

export {
  actionService,
  companyService,
  categoryService,
  configService,
  errorService,
  reviewService,
  utilService,
  userService
}