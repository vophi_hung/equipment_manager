import {
  Review
} from '@/models'
import CrudService from './crudService'

export default class ReviewService extends CrudService {
  constructor() {
    super(Review)
    
  }
}