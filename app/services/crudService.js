export default class CrudControler {
  constructor(Model) {
    this.Model = Model
  }

  async count(params) {
    return await this.Model.count({
      where: params
    })
  }
 
  async create(params) {
    return await this.Model.create(params)
  }

  async delete(params) {
    const {
      id
    } = params

    const item = await this.Model.findById(id)

    return await item.destroy(params)
  }

  async getItem(params) {
    return await this.Model.findOne({
      where: params
    })
  }

  _buildSearchObject(keyword) {
    const fields = this.searchFields || []

    const searchLikes = []

    fields.forEach(field => {
      const searchObj = {}
      searchObj[field] = {
        '$like': `%${keyword.toUpperCase()}%`
      } 
      searchLikes.push(searchObj)
    })

    return searchLikes;
  }

  async getList(params = {}, options = {limit: 10, offset: 0, order: [] }) {
    params = Object.assign(params, options.filter)
    if (options.search && options.search !== '') {
      params['$or'] = this._buildSearchObject(options.search)
    }

    return await this.Model.findAndCountAll({
      where: params,
      limit: options.limit,
      offset: options.offset,
      order: options.order
    })
  }

  async search(keyword) {
    const searchField = this.searchField || {}
    const keys = Object.keys(searchField)
    if (keys.length > 0) {
      //build search query
      let searchString = keys.map(key => {
        return `upper(${key}) similar to upper(:${key})`
      })

      searchString = searchString.join(' and ')
      //build replacement
      const replacement = {}
      keys.forEach(key => {
        replacement[key] = `%${keyword}%`
      })

      
    }
  }

  async update(params) {
    const {
      id
    } = params

    const item = await this.Model.findById(id)
    await item.update(params)
    return this.getItem({ id })
  }

}
