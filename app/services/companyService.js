import {
  Company
} from '@/models'
import {
  sequelize
} from '@/models/base'
import CrudService from './crudService'

export default class CompanyService extends CrudService {
  constructor() {
    super(Company)
    this.searchFields = ['name_upper']
  }

  async search(keyword) {
    return await sequelize.query(`select * from tbl_company where name_upper similar to upper(:keyword) order by name asc limit 10`, { 
      replacements: { keyword: `%${keyword}%` },
      type: sequelize.QueryTypes.SELECT
    })
  }
}